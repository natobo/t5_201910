package model.data_structures;

import java.util.Iterator;

import junit.framework.TestCase;

public class TestMaxColaPrioridad extends TestCase{

	/**
	 * Es la cola de strings donde se har�n las pruebas
	 */
	private MaxColaPrioridad<String>  colaStrings;
	/**
	 * Es la cola de Integers donde se har�n las pruebas
	 */
	private MaxColaPrioridad<Integer> colaIntegers;
	
	public void setUp1() 
	{
		try
		{
			//Crea la cola de enteros
			colaIntegers=new MaxColaPrioridad<Integer>("");
			//A�ade elementos a la cola de enteros.
			colaIntegers.agregar(1);
			colaIntegers.agregar(2);
			colaIntegers.agregar(3);
			colaIntegers.agregar(4);
			colaIntegers.agregar(5);
			colaIntegers.agregar(6);
			
			//Crea la cola de strings
			colaStrings=new MaxColaPrioridad<String>("");
			//A�ade elementos a la cola de strings.
			colaStrings.agregar("a");
			colaStrings.agregar("b");
			colaStrings.agregar("c");
			colaStrings.agregar("d");
			colaStrings.agregar("e");
			colaStrings.agregar("f");
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			fail("Las colas no se han podido inicializar");
		}
	}
	
	public void setUp2() 
	{
		try
		{
			//Crea la cola de enteros
			colaIntegers=new MaxColaPrioridad<Integer>("");
			//A�ade elementos a la cola de enteros.
			colaIntegers.agregar(4);
			colaIntegers.agregar(5);
			colaIntegers.agregar(2);
			colaIntegers.agregar(1);
			colaIntegers.agregar(3);
			colaIntegers.agregar(6);

			//Crea la cola de strings
			colaStrings=new MaxColaPrioridad<String>("");
			//A�ade elementos a la cola de strings.
			colaStrings.agregar("f");
			colaStrings.agregar("b");
			colaStrings.agregar("d");
			colaStrings.agregar("e");
			colaStrings.agregar("a");
			colaStrings.agregar("c");
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			fail("Las colas no se han podido inicializar");
		}
	}
	
	public void setUp3(){
		try{
			colaIntegers = new MaxColaPrioridad<Integer>("");
			colaIntegers.agregar(2);
			
			colaStrings = new MaxColaPrioridad<String>("");
			colaStrings.agregar("c");
		}
		catch(Exception e){
			fail("Las colas no se han podido inicializar");
		}
	}
	
	public void testDarNumElementos(){
		setUp1();
		
		assertTrue("No esta retornando el tama�o adecuado", colaIntegers.darNumElementos() == 6);
		assertTrue("No esta retornando el tama�o adecuado", colaStrings.darNumElementos() == 6);

		//Elimina un elemento para comprobar si cambia el tama�o
		colaStrings.delMax();
		colaIntegers.delMax();

		assertTrue("No esta retornando el tama�o adecuado", colaIntegers.darNumElementos() == 5);
		assertTrue("No esta retornando el tama�o adecuado", colaStrings.darNumElementos() == 5);
	}
	
	public void testAgregar(){
		setUp1();
		
		colaIntegers.agregar(10);
		
		Iterator<Integer> itInt = colaIntegers.iterator();
		boolean loEncontro = false;
		while(itInt.hasNext())
		{
			int x = itInt.next();
			if(x == 10)
				loEncontro = true;
		}
		assertTrue("Deberia haber encontrado el elemento", loEncontro);
		
		colaStrings.agregar("z");
		
		Iterator<String> itStr = colaStrings.iterator();
		boolean loEncontro2 = false;
		while(itStr.hasNext())
		{
			String s = itStr.next();
			if(s.equalsIgnoreCase("z"))
				loEncontro2 = true;
		}
		assertTrue("Deberia haber encontrado el elemento", loEncontro2);

		//Para verificar que la cola respeta la prioridad
		setUp3();
		
		colaIntegers.agregar(1);
		
		assertTrue("Deberia haber encontrado el elemento", colaIntegers.max() == 1);
		
		colaStrings.agregar("a");
				
		assertTrue("Deberia haber encontrado el elemento", colaStrings.max().equals("a"));

	}
	
	public void testDelMax(){
		setUp1();
		
		assertTrue("No retorna el numero adecuado", colaIntegers.delMax() == 1);
		assertTrue("No retorna la letra adecuada", colaStrings.delMax().equals("a"));

		assertTrue("No esta retornando el tama�o adecuado", colaIntegers.darNumElementos() == 5);
		assertTrue("No esta retornando el tama�o adecuado", colaStrings.darNumElementos() == 5);
		
		setUp2();
		
		assertTrue("No retorna el numero esperado", colaIntegers.delMax() == 1);
		assertTrue("No retorna la letra esperada", colaStrings.delMax().equals("a"));

		assertTrue("No esta retornando el tama�o adecuado", colaIntegers.darNumElementos() == 5);
		assertTrue("No esta retornando el tama�o adecuado", colaStrings.darNumElementos() == 5);
	}
	
	public void testMax(){
		setUp1();
		
		assertTrue("No retorna el numero adecuado", colaIntegers.delMax() == 1);
		assertTrue("No retorna la letra adecuada", colaStrings.delMax().equals("a"));

		setUp2();
		
		assertTrue("No retorna el numero esperado", colaIntegers.delMax() == 1);
		assertTrue("No retorna la letra esperada", colaStrings.delMax().equals("a"));
	}
	
	public void testEsVacia(){
		colaIntegers = new MaxColaPrioridad<Integer>("");
		colaIntegers.agregar(1);
	setUp1();
	
	assertFalse("Deberia ser falso", colaIntegers.esVacia());
	assertFalse("Deberia ser falso", colaStrings.esVacia());

	colaIntegers.delMax();
	colaIntegers.delMax();
	colaIntegers.delMax();
	colaIntegers.delMax();
	colaIntegers.delMax();
	colaIntegers.delMax();

	colaStrings.delMax();
	colaStrings.delMax();
	colaStrings.delMax();
	colaStrings.delMax();
	colaStrings.delMax();
	colaStrings.delMax();

	assertTrue("Deberia ser verdadero ",colaIntegers.esVacia());
	assertTrue("Deberia ser verdadero",colaStrings.esVacia());
		
	}
}
