package model.data_structures;

import java.util.Iterator;

import model.util.Sort;

public class MaxColaPrioridad <T extends Comparable<T>> implements IMaxPQ<T>
{
	/**
	 *Cola base 
	 */
	private IQueue<T> cola;
	
	/**
	 * Tipo de prioridad 
	 */
	private String prioridad;
	
	/**
	 * Constructor
	 * @param tipoPrioridad tipo de prioridad de la cola
	 */
	public MaxColaPrioridad(String tipoPrioridad)
	{
		cola = new Queue<T>();
		prioridad = tipoPrioridad;
	}
	
	public Iterator<T> iterator()
	{
		return cola.iterator();
	}
	
	@Override
	public int darNumElementos() {
		// TODO Auto-generated method stub
		return cola.size();
	}

	@Override
	public void agregar(T t) 
	{
		// TODO Auto-generated method stub
		cola.enqueue(t);
		Comparable[] lista = Sort.getArreglo(cola);
		Sort.ordenarMergeSort(lista, prioridad);
		cola = new Queue<T>();
		for(int i = 0; i < lista.length; i++)
		{			
			cola.enqueue((T) lista[i]);
		}
	}

	@Override
	public T delMax() 
	{
		// TODO Auto-generated method stub
		return cola.dequeue();
	}

	@Override
	public T max() 
	{
		// TODO Auto-generated method stub
		IQueue<T> x = cola;
		return x.dequeue();
	}

	@Override
	public boolean esVacia() 
	{
		// TODO Auto-generated method stub
		return cola.isEmpty();
	}
}
