package model.data_structures;

public interface IMaxPQ<T> 
{
	/**
	 * Retorna el numero de elementos contenidos
	 * @return el numero de elemntos contenidos
	 */
	public int darNumElementos();
	/**
	 * Agrega un elemento a la cola. Si el elemento ya existe y tiene una prioridad diferente, el elemento debe actualizarse en la cola de prioridad.
	 * @param t el nuevo elemento que se va ha agregar
	 */
	public void agregar(T t);
	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retorna; <br>
	 * null en caso de cola vac�a
	 * @return Elemento maximo de la cola.
	 */ 
	public T delMax ();
	/**
	 * Obtener el elemento m�ximo (sin sacarlo de la Cola);<br> 
	 * null en caso de cola vac�a
	 */
    public T max() ;
    
    /**
     * Retorna si la cola est� vac�a o no
     * @return True si esta vacia, false de lo contrario.
     */
    public boolean esVacia () ;
    
}
