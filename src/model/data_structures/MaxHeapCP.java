package model.data_structures;


public class MaxHeapCP  <T extends Comparable<T>> implements IMaxPQ<T>
{
	/**
	 * Arreglo dinamico con los elementos. 
	 */
	private ArregloDinamico<T> elementos;   
	/**
	 * Tama�o del Heap.
	 */
	private int tamano;

	/**
	 * Constructor del heap
	 * @param a arreglo con los elementos qe
	 */
	public MaxHeapCP(int tamanoMax,Class<T> tclass) 
	{
		// Se le suma 1 porque no se usa la posicion 0 del arreglo
		int num=tamanoMax+1;
		elementos = new ArregloDinamico<T>(num,tclass);
	}

	/**
	 * Dar arreglo dinamico del Heap
	 */
	public ArregloDinamico<T> darArreglo()
	{
		return elementos;
	}

	@Override
	public int darNumElementos() 
	{
		// TODO Auto-generated method stub
		return tamano;
	}

	@Override
	public void agregar(T t)
	{
		elementos.agregar(t);	
		tamano=elementos.darTamano();
		swim(tamano);
	}

	@Override
	public T delMax() 
	{
		// TODO Auto-generated method stub
		 T max = elementos.darElemento(1);          
		 elementos.exchange(1, tamano--);             
		 elementos.eliminarPorPosicion(tamano+1);
         sink(1);                   
        return max; 
	}

	@Override
	public T max() 
	{
		// TODO Auto-generated method stub
		return elementos.darElemento(1);
	}

	@Override
	public boolean esVacia() {
		// TODO Auto-generated method stub
		return tamano==0;
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private boolean less(int i, int j)
	{
		 return elementos.darElemento(i).compareTo(elementos.darElemento(j)) < 0; 
	}
	/**
	 * Recorre el Heap desde los ultimos niveles hasta la raiz verificando que se cumpla el orden del heap
	 * @param k index del arreglo
	 */
	private void swim(int k) 
	{   
		while (k > 1 && less(k/2, k))   
		{     
			elementos.exchange(k/2, k);     
			k = k/2;   
		} 
	}
	/**
	 * Recorre el Heap desde la raiz hasta la raiz veri que se cumpla el orden del heap
	 * @param k index del arreglo
	 */
	private void sink(int k) 
	{   
		while (2*k <= tamano)   
		{      
			int j = 2*k;     
			if (j < tamano && less(j, j+1)) 
				j++;      
			if (!less(k, j)) 
				break;      
			elementos.exchange(k, j);     
			k = j;   
		}
	}
  }

