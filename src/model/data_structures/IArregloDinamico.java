package model.data_structures;

public interface IArregloDinamico <T>
{
	/**
	 * Retornar el numero de elementos en el arreglo
	 * @return
	 */
	int darTamano( );
	
	/**
	 * Retornar el elemento en la posicion i
	 * @param i posicion de consulta
	 * @return elemento de consulta. null si no hay elemento en posicion.
	 */
	T darElemento( int i );

	/**
	 * Agregar un dato de forma compacta (en la primera casilla disponible) 
	 * Caso Especial: Si el arreglo esta lleno debe aumentarse su capacidad, agregar el nuevo dato y deben quedar multiples casillas disponibles para futuros nuevos datos.
	 * @param elemento nuevo elemento
	 */
	public void agregar( T elemento );
		
	/**
	 * Buscar un elemento en el arreglo.
	 * @param elemento Objeto de busqueda en el arreglo
	 * @return elemento encontrado en el arreglo (si existe). null si no se encontro el dato.
	 */
	T buscar(T elemento);
	
	/**
	 * Eliminar un dato del arreglo.
	 * Los elementos restantes deben quedar "compactos" desde la posicion 0.
	 * @param elemento Objeto de eliminacion en el arreglo
	 * @return elemento eliminado
	 */
	T eliminar( T elemento );

}
