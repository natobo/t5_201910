package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.IteratorTaller;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 5----------------------");
		System.out.println("1. Cargar datos de infracciones en movimiento");
		System.out.println("2. Obtener una muestra de datos a ordenar");
		System.out.println("3. Obtener muestra de objetos LocationVO a partir de una muestra de datos.");
		System.out.println("4. Agregar muestras de LocationVO a una cola de prioridad, mostrando el tiempo del proceso.");
		System.out.println("5. Agregar muestras de LocationVO a un Heap, mostrando el tiempo del proceso.");
		System.out.println("6. Aplicar la operacion delMax() en una cola de prioridad, mostrando el tiempo del proceso.");
		System.out.println("7. Aplicar la operacion delMax() en un Heap, mostrando el tiempo del proceso.");
	    System.out.println("8. Mostrar v�as mas importantes por su n�meros de accidentes (de mayor a menor) con una cola ordenada.");
	    System.out.println("9. Mostrar v�as mas importantes por su n�meros de accidentes (de mayor a menor) con un heap.");
	    System.out.println("10.Salir");
		
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printDatosMuestra(  Comparable [ ] muestra)
	{
		for ( Comparable elemento : muestra)
		{	
			if(elemento!=null)
			System.out.println(  elemento.toString() );    
		}
	}
	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}
	
}
