package controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;
import com.opencsv.CSVReader;
import model.data_structures.IQueue;
import model.data_structures.IteratorTaller;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.data_structures.Queue;
import model.util.Sort;
import model.vo.LocationVO;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller 
{

	private MovingViolationsManagerView view;

	/**
	 * Lista de meses del programa
	 */
	private String[] listaMes=new String[4];
	/**
	 * Cola donde se carga las infracciones del  cuatrimestre.
	 */
	private Queue<VOMovingViolations> colaInfracciones;

	/**
	 *  Muestra obtenida de los datos cargados. 
	 */
	Comparable<VOMovingViolations> [ ] muestraInfracciones;
	/**
	 * Muestra de localizaciones donde se producen infracciones
	 */
	Comparable<LocationVO>[] muestraLocaciones;
	/**
	 * Heap que contiene objetos locations
	 */
	private MaxHeapCP<LocationVO> heapLocations;
	/**
	 * Cola que contiene objetos locations.
	 */
	private MaxColaPrioridad<LocationVO> colaLocations;
	/**
	 * numero total de infracciones
	 */
	private int numTotalInfrac;


	/**
	 * Constructor del controlador
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();

		listaMes[0]="January";
		listaMes[1]="February";
		listaMes[2]="March";
		listaMes[3]="April";

		colaInfracciones=new Queue<VOMovingViolations>();
		heapLocations=new MaxHeapCP<>(50000, LocationVO.class);
		colaLocations=new MaxColaPrioridad<>("");

		numTotalInfrac=0;
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		long startTime;
		long endTime;
		long duration;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();
			int nMuestra=0;

			switch(option)
			{
			case 1:

				view.printMessage("Cargando los datos...");

				loadMovingViolations();

				view.printMessage("Se cargaron los datos.");

				break;

			case 2:

				view.printMessage("Dar tamano de la muestra: ");
				nMuestra = sc.nextInt();
				muestraInfracciones = this.generarMuestra( nMuestra );
				view.printMessage("Muestra generada");

				break;


			case 3:

				generarMuestraLocationsVo();
				view.printMessage("Se genero una muestra de LocationsVO a partir de la muestra de Infracciones ");

				break;


			case 4:

				if (  muestraLocaciones != null  )
				{
					startTime = System.currentTimeMillis();

					agregarMaxCola();

					endTime = System.currentTimeMillis();
					duration = endTime - startTime;

					view.printMessage("Tiempo para agregar una muestra LocationVo en una cola de prioridad: " + duration + " milisegundos");
				}
				else
				{
					view.printMessage("Muestra invalida");
				}
				break;

			case 5:	
				if (  muestraLocaciones != null  )
				{
					startTime = System.currentTimeMillis();

					agregarMaxHeap();

					endTime = System.currentTimeMillis();
					duration = endTime - startTime;

					view.printMessage("Tiempo para agregar una muestra LocationVo en un heap: " + duration + " milisegundos");
				}
				else
				{
					view.printMessage("Muestra invalida");
				}
				break;

			case 6:	
				if (  muestraLocaciones != null  )
				{
					startTime = System.currentTimeMillis();

					aplicarDelMaxCola();

					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Tiempo aplicando delMax() en una cola ordenada: " + duration + " milisegundos");
					view.printMessage("La cola quedo vacia.");
				}
				else
				{
					view.printMessage("Muestra invalida");
				}
				break;

			case 7:	
				if (  muestraLocaciones != null  )
				{
					startTime = System.currentTimeMillis();

					aplicarDelMaxHeap();

					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Tiempo aplicando delMax() en un heap: " + duration + " milisegundos");
					view.printMessage("El heap quedo vacio");
				}
				else
				{
					view.printMessage("Muestra invalida");
				}
				break;

			case 8:
				if (  muestraLocaciones != null  )
				{

					LocationVO[] elements=reqMaxCola();
					int cont=0;

					for (int i = 0; i < elements.length&&cont<20; i++) 
					{
						if(elements[i]!=null)
							System.out.println("La ubicacion: "+elements[i].getLocation()+" con address Id: "+elements[i].getAddressId()+" tiene "+elements[i].getNumberOfRegisters()+" infracciones registradas");
						cont++;
					}
				}
				
				else
				{
					view.printMessage("Muestra invalida");
				}
				break;

			case 9:
				if (  muestraLocaciones != null  )
				{

					LocationVO[] elements=reqMaxHeap();
					int cont=0;

					for (int i = 0; i < elements.length&&cont<20; i++) 
					{
						if(elements[i]!=null)
							System.out.println("La ubicacion: "+elements[i].getLocation()+" con address Id: "+elements[i].getAddressId()+" tiene "+elements[i].getNumberOfRegisters()+" infracciones registradas");
						cont++;
					}
				}
				
				else
				{
					view.printMessage("Muestra invalida");
				}
				break;
				
			case 10:	
				fin=true;
				sc.close();
				break;
			}
		}

	}
	/**
	 * 	Carga los csv del cuatrimestre. 
	 */
	public void loadMovingViolations() 
	{		
		colaInfracciones=new Queue<VOMovingViolations>();
		numTotalInfrac=0;

		//Elimina todo rastro de la pila anterior
		Runtime garbage = Runtime.getRuntime();
		garbage.gc();

		Queue<VOMovingViolations> colaCargada=new Queue<VOMovingViolations>();
		try 
		{
			int x=0;
			int numTotalInfracciones=0;

			for (int i = 0; i < 4; i++) 
			{
				String xMes=listaMes[x];
				String nombreMes=xMes;
				CSVReader csvReader = new CSVReader(new FileReader("."+File.separator+"data"+File.separator+"Moving_Violations_Issued_in_"+nombreMes+"_2018.csv"));
				String[] fila=null;
				csvReader.readNext();
				int numInfrMes=0;
				while ((fila=csvReader.readNext()) != null) 
				{
					//Crea la infraccion.
					VOMovingViolations infraccion=new VOMovingViolations(fila[0], fila[15], fila[2],fila[9], fila[12], convertirFecha_Hora_LDT(fila[13]),fila[14],fila[8], fila[4],fila[3], fila[10],fila[11]);

					//La agrega a la pila
					numInfrMes++;
					colaCargada.enqueue(infraccion);
				}

				System.out.println("El numero de infracciones del mes "+xMes+" es:"+numInfrMes);

				numTotalInfracciones+=numInfrMes;
				x++;
				csvReader.close();
			} 


			System.out.println("El numero total de infracciones de cuatrimestre es: "+numTotalInfracciones+" infracciones");

			numTotalInfrac+=numTotalInfracciones;

			colaInfracciones=colaCargada;


		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public Comparable<VOMovingViolations> [ ] generarMuestra( int n )
	{
		muestraInfracciones = new Comparable[ n ];

		// TODO Llenar la muestra aleatoria con los datos guardados en la estructura de datos
		IteratorTaller<VOMovingViolations> it= colaInfracciones.iterator();

		int i=0;
		///Inserta los elementos de la cola en la muestra

		while(it.hasNext()&&i<n)
		{
			VOMovingViolations element=it.next();
			muestraInfracciones[i]=element;
			i++;
		}

		//Baraja los elementos de la muestra con un orden aleatorio

		int index=n-1;
		Comparable<VOMovingViolations> temp;
		for (int j=index; j >1; j--) 
		{
			int aleatorio=(int) (Math.random()*j);  //Da un numero aleatorio entre 0 y n.
			temp=muestraInfracciones[j];   //Hace una variable temporal para guardar el elemento j.
			muestraInfracciones[j]=muestraInfracciones[aleatorio];  //Asigna a la posicion J un elemento aleatorio.
			muestraInfracciones[aleatorio]=temp;   //Guarda en la posicion j el elemento aleatorio.
		}

		return muestraInfracciones;
	}


	/**
	 * Genera una muestra de objetos LocationVO a partir de una muestra de datos
	 */ 
	public void generarMuestraLocationsVo()
	{
		//Peor de los casos todas las infracciones tienen diferentes ubicaciones.
		muestraLocaciones= new Comparable [muestraInfracciones.length];

		int indexLocaciones=0;

		Sort.ordenarQuickSort(muestraInfracciones, "addressId");

		LocationVO tmp=null;

		for (int i = 0; i < muestraInfracciones.length; i++) 
		{
			VOMovingViolations elemento= (VOMovingViolations) muestraInfracciones[i];
			if(!elemento.getAddressId().equals(""))
			{
				if(tmp==null||Integer.parseInt(elemento.getAddressId())!=tmp.getAddressId())
				{
					tmp=new LocationVO(Integer.parseInt(elemento.getAddressId()),elemento.getLocation());
					muestraLocaciones[indexLocaciones]=tmp;
					indexLocaciones++;
				}
				else
				{
					tmp.aumentarRegistro();
				}
			}
		}
		
		for (int i = 0; i < 20; i++) 
		{
			System.out.println( muestraLocaciones[i]);
		}

	}

	/**
	 * Metodo que agrega la muestra de LocationsVo a un Heap
	 */
	public void agregarMaxHeap()
	{
		for (int i = 0; i < muestraLocaciones.length; i++) 
		{
			if(muestraLocaciones[i]!=null)
				heapLocations.agregar((LocationVO) muestraLocaciones[i]);
		}
	}

	/**
	 * Metodo que agrega la muestra de LocationsVo a un Heap
	 */
	public void agregarMaxCola()
	{
		for (int i = 0; i < muestraLocaciones.length; i++) 
		{
			if(muestraLocaciones[i]!=null)
				colaLocations.agregar((LocationVO) muestraLocaciones[i]);
		}
	}

	/**
	 * Metodo que aplica delMax() a todos los elementos de una cola.
	 */
	public void aplicarDelMaxCola()
	{
		for (int i = 0; i < colaLocations.darNumElementos(); i++) 
		{
			colaLocations.delMax();
		}	
	}

	/**
	 * Metodo que aplica delMax() a todos los elementos en un heap.
	 */
	public void aplicarDelMaxHeap()
	{
		for (int i = 0; i < heapLocations.darNumElementos(); i++) 
		{
			heapLocations.delMax();
		}	
	}

	/**
	 * Metodo que responde al Requerimento funcional usando una cola ordenada.
	 */
	public LocationVO[] reqMaxCola()
	{
		LocationVO[] rta= new LocationVO[colaLocations.darNumElementos()];
		for (int i = 0; i < colaLocations.darNumElementos(); i++) 
		{
			rta[i]=colaLocations.delMax();
		}	
		return rta;
	}
	/**
	 * Metodo que responde al Requerimento funcional usando un heap.
	 */
	public LocationVO[] reqMaxHeap()
	{
		LocationVO[] rta= new LocationVO[heapLocations.darNumElementos()];
		for (int i = 0; i < heapLocations.darNumElementos(); i++) 
		{
			rta[i]=heapLocations.delMax();
		}	
		return rta;
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha);
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}

}
